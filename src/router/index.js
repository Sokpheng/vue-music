import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import store from "../store";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/manage",
    name: "Manage",
    meta: { requireAuth: true },
    component: () =>
      import(/* webpackChunkName: "manage" */ "../views/Manage.vue"),
  },
  {
    path: "/song/:id",
    name: "Song",
    component: () => import(/* webpackChunkName: "song" */ "../views/Song.vue"),
  },
  {
    path: "/:pathMatch(.*)*",
    redirect: { name: "Home" },
  },
  // {
  //   path: '/:pathMatch(.*)*',
  //   name: '404',
  //   component: () =>
  // 		import(/* webpackChunkName: "404" */ '../views/404.vue'),
  // }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  // customize the active link style
  linkExactActiveClass: "text-yellow-500",
});

// This is the global route guard to perform every route requests
router.beforeEach((to, from, next) => {
  // check if the routs meta is not true, let's it countinue
  if (!to.matched.some((record) => record.meta.requireAuth)) {
    next();
    return;
  }

  if (store.state.auth.userLoggedIn) {
    next();
  } else {
    next({ name: "Home" });
  }
});

export default router;

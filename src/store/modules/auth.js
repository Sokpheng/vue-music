import { auth, userCollection } from "../../plugin/firebase";

export default {
	state: {
		authModalShow: false,
		// to keep tracking if the user is loggedIn
		userLoggedIn: false,
	},
	mutations: {
		toggleAuthModal(state) {
			state.authModalShow = !state.authModalShow;
		},
		toggleAuthUser(state) {
			state.userLoggedIn = !state.userLoggedIn;
		},
	},
	actions: {
		//#region Register
		async register({commit}, payload) {
			const userCred = await auth.createUserWithEmailAndPassword(
				payload.email,
				payload.password
			);

			await userCollection.doc(userCred.user.uid).set({
				name: payload.name,
				email: payload.email,
				age: payload.age,
				country: payload.country,
			});

			await userCred.user.updateProfile({displayName: payload.name});
			commit('toggleAuthUser');
		},
		//#endregion

		//#region login
		async login({commit}, payload) {
			await auth.signInWithEmailAndPassword(payload.email, payload.password);
			commit('toggleAuthUser');
		},
		//#endregion

		//#region logOut
		async logout({commit}) {
			await auth.signOut();
			commit('toggleAuthUser');
			// window.location.reload();
		},
		//#endregion

		//#region initLogin
		initLogin({commit}) {
			// get the current user token
			const user = auth.currentUser;

			if (user) {
				commit('toggleAuthUser');
			}
		},
		//#endregion
	},
	getters: {
		authModalShow: (state) => {
			return state.authModalShow;
		},
	},
};

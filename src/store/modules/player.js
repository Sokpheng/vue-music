// import {Howl} from 'howler'; // installed dependency
import {Howl} from 'howler';
import helper from '../../plugin/helper';
export default {
	state: {
		// Current playing song
		currentSong: {},
		sound: {},
		seek: '00:00',
		duration: '00:00',
		playerProgress: '0%',
	},
	mutations: {
		newSong(state, payload) {
			state.currentSong = payload;

			state.sound = new Howl({
				src: [payload.url],
				html5: true,
			});
		},

		updatePosition(state) {
			state.seek = helper.formatTime(state.sound.seek());
			state.duration = helper.formatTime(state.sound.duration());
			state.playerProgress = `${
				(state.sound.seek() / state.sound.duration()) * 100 + '%'
			}`;
		},
	},
	actions: {
		//#region newSong
		async newSong({commit, state, dispatch}, payload) {
			// unload or distroy sound on the memory to avoid duplication playing
			if (state.sound instanceof Howl) {
				state.sound.unload();
			}

			commit('newSong', payload);
			state.sound.play();

			state.sound.on('play', () => {
				requestAnimationFrame(() => {
					dispatch('progress');
				});
			});
		},
		//#endregion

		//#region progress
		progress({commit, state, dispatch}) {
			commit('updatePosition');

			if (state.sound.playing()) {
				requestAnimationFrame(() => {
					dispatch('progress');
				});
			}
		},
		//#endregion

		//#region toggleAudio
		async toggleAudio({state}) {
			// .player is a Howler prop return boolean
			if (!state.sound.playing) {
				return;
			}

			// .playing() is Howler function
			if (state.sound.playing()) {
				state.sound.pause();
			} else {
				state.sound.play();
			}
		},
		//#endregion

		//#region updateSeek
		updateSeek({state, dispatch}, payload) {
			if (!state.sound.playing) {
				return;
			}

			const {x, width} = payload.currentTarget.getBoundingClientRect();
			// Document = 2000, Timeline = 1000, Click = 500, Distance = 500
			const clickX = payload.clientX - x;
			const percentage = clickX / width;
			const seconds = state.sound.duration() * percentage;

			state.sound.seek(seconds);

			state.sound.once('seek', () => {
				dispatch('progress');
			});
		},
		//#endregion
	},
	getters: {
		// for stracking and changing the play/pause button
		playing: (state) => {
			if (state.sound.playing) {
				return state.sound.playing();
			}
			return false;
		},
	},
};

import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VeeValidationPlugin from "./plugin/validation";
import { auth } from "./plugin/firebase";
import Icon from "./directive/icon";
import "./assets/tailwind.css";
import "./assets/main.css";
import i18n from "./plugin/i18n";
import "./registerServiceWorker";
import GlobalComponents from './plugin/_globals'
import ProgressBar from './plugin/progressBar'
import 'nprogress/nprogress.css'

ProgressBar(router);
 
let app = null;
auth.onAuthStateChanged(() => {
  if (!app) {
    app = createApp(App);

    app.use(i18n);
    app.use(store);
    app.use(router);
    app.use(VeeValidationPlugin);
    app.use(GlobalComponents);
    app.directive("icon", Icon);
    app.mount("#app");
  }
});

import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyA1Abu47DV2qdo8nKa9P8ntwR0dX07DhGg",
  authDomain: "music-62bac.firebaseapp.com",
  projectId: "music-62bac",
  storageBucket: "music-62bac.appspot.com",
  appId: "1:139314981889:web:7075b0d89b430c0097e400",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();
const db = firebase.firestore();
const userCollection = db.collection("users");
const songCollection = db.collection("songs");
const commentCollection = db.collection("comments");
const storage = firebase.storage();

export { auth, db, userCollection, songCollection, commentCollection, storage };

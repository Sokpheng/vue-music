import {
  defineRule,
  Field as VeeField,
  Form as VeeForm,
  ErrorMessage,
  configure,
} from "vee-validate";
import {
  required,
  min,
  min_value as minValue,
  max,
  max_value as maxValue,
  email,
  alpha_spaces as alphaSpaces,
  confirmed,
  not_one_of as exclude,
} from "@vee-validate/rules";
export default {
  install(app) {
    app.component("VeeForm", VeeForm);
    app.component("VeeField", VeeField);
    app.component("ErrorMessage", ErrorMessage);

    defineRule("required", required);
    defineRule("tos", required);
    defineRule("min", min);
    defineRule("min_value", minValue);
    defineRule("mex_value", maxValue);
    defineRule("max", max);
    defineRule("alphaSpaces", alphaSpaces);
    defineRule("email", email);
    defineRule("passwordMisMatch", confirmed);
    defineRule("countryExcluded", exclude);

    //Overwrite the error messages
    configure({
      generateMessage: (ctx) => {
        const messages = {
          required: `The field ${ctx.field} is required.`,
          min: `The field ${ctx.field} is too short.`,
          max: `The field ${ctx.field} is too long.`,
          min_value: `The field ${ctx.field} is too low.`,
          maxValue: `The field ${ctx.field} is too high.`,
          alphaSpaces: `The field ${ctx.field} may only contain alphabetical character.`,
          email: `The field ${ctx.field} must be a valid email.`,
          passwordMisMatch: `The field ${ctx.field} don't match.`,
          countryExcluded: `Due to restriction, we do not accept users from the location.`,
          tos: `You must accept Terms of Service.`,
        };
        const message = messages[ctx.rule.name]
          ? messages[ctx.rule.name]
          : `The field ${ctx.field} is invalid.`;
        return message;
      },
      validateOnBlur: true,
      validateOnChange: true,
      validateOnInput: false,
      validateOnModelUpdate: true,
    });
  },
};

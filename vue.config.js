module.exports = {
  // custom this prop to prevent error path not found in npm run build
	publicPath:
		process.env.NODE_ENV === 'production' ? '' : '/',

	pluginOptions: {
		i18n: {
			locale: 'en',
			fallbackLocale: 'en',
			localeDir: 'locales',
			enableLegacy: true,
		},
	},
};
